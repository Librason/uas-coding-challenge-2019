/*
Goal: find intersection points of a circle and a line
Author: Librason
Email: librason@students.cs.ubc.ca
Algorithm: circle equation: (x-a)^2 + (y-b)^2 = R^2
           linear line equation: y = kx + h
		   intersection points can be found: (k^2+1)x^2 + (2k(h-b)-2a)x + (h-b)^2 + a^2 - R^2 = 0,
		   solve for x

		   Special case: line is vertical, then see whether the line is in range a - R <= x <= a + R
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int FindLinearEqn(double y1, double x1, double y0, double x0, double* k, double* h);
void SolveForX(double temp1, double temp2, double temp3, double k, double h);

int main(void) {
	double a, b, R; //for circle equation
	double y1, x1, y0, x0, k, h; //for linear equation
	double temp1, temp2, temp3; //for solving x in circle and linear equation 

	printf("Enter the center of the circle in the form of <x y> separate by space: ");
	scanf("%lf %lf", &a, &b);
	printf("Enter the radius of the circle: ");
	scanf("%lf", &R);

	printf("Enter two points on the line in the form of <x y> separate by space\n");
	printf("the first point: ");
	scanf("%lf %lf", &x0, &y0);
	printf("the second point: ");
	scanf("%lf %lf", &x1, &y1);

	//check special case
	int CheckVertical = FindLinearEqn(y1, x1, y0, x0, &k, &h);
	if (CheckVertical == 1) // special case for vertical line
	{
		if (x0 < (a - R) || x0 > (a + R))
		{
			printf("There is no intersection.\n");
		}
		if (x0 == (a - R) || x0 == (a + R)) {
			printf("There is one intersection on point (%.3lf,%.3lf).\n", x0, b);
		}
		if(x0 > (a - R) && x0 < (a + R))
		{
			double temp = pow(R, 2) - pow((x0 - a), 2);
			double soln1 = sqrt(temp) + b;
			double soln2 = -sqrt(temp) + b;
			printf("There are two intersection points.\n");
			printf("(%.3lf,%.3lf) and (%.3lf,%.3lf)\n", x0, soln1, x1, soln2);
		}
		system("pause");
		return 0;
	}

	temp1 = pow(k, 2) + 1;
	temp2 = 2 * k * (h - b) - 2 * a;
	temp3 = pow(h - b, 2) + pow(a, 2) - pow(R, 2);

	SolveForX(temp1, temp2, temp3, k, h);

	system("pause");
	return 0;
	
}


int FindLinearEqn(double y1, double x1, double y0, double x0, double* k, double* h){
	if (x0 == x1) //the line is vertical 
	{
		return 1;
	}
	*k = (y1 - y0) / (x1 - x0); //slope
	*h = y0 - *k * x0; //y-intersection of the line
	return 0;
}

void SolveForX(double temp1, double temp2, double temp3, double k, double h) {
	double epsilon = 0.0001; //for comparing if two doubles are the same
	double temp4 = pow(temp2, 2) - 4 * temp1 * temp3;

	if (temp4 < 0)
	{
		printf("There is no intersection.\n");
	}
	else if(temp4 >= 0 && temp4 <= epsilon)
	{
		double soln;
		double y;
		soln = ((-temp2) + sqrt(temp4)) / (2 * temp1);
		y = k * soln + h;
		printf("There is one intersection on point (%.3lf,%.3lf).\n", soln, y);
	}
	else
	{
		double soln1, soln2;
		soln1 = ((-temp2) + sqrt(temp4)) / (2 * temp1);
		soln2 = ((-temp2) - sqrt(temp4)) / (2 * temp1);

		double y1, y2;
		y1 = k * soln1 + h;
		y2 = k * soln2 + h;

		printf("There are two intersection points.\n");
		printf("(%.3lf,%.3lf) and (%.3lf,%.3lf)\n", soln1, y1, soln2, y2);
	}

}